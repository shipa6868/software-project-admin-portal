import React from 'react';
import { Layout } from 'antd';

const { Footer } = Layout;

const CustomFooter = () => {
    const style = {
        textAlign: 'center'
    }
    return (
        <Footer style={style}>Mave ©2019 Created by Mave.</Footer>
    );
}

export default CustomFooter;