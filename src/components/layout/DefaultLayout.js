import React, { useState, lazy, Suspense } from 'react';
import { Layout, Button } from 'antd';
import LoadingSuspense from '../common/LoadingSuspense';
import CustomFooter from './Footer';
import NavHeader from './header/NavHeader';
import { Route, Redirect, Switch } from 'react-router-dom';
import Routes from '../../routes/Routes';
import { ROOT_PATH, DASHBOARD_PATH } from '../../routes/Slugs';
import { NavsForCustomer, NavsForSeller } from '../../helpers/Navs';
const AsideLeft = lazy(() => import('./AsideLeft'));
const ButtonGroup = Button.Group;

const { Sider, Content } = Layout;

const DefaultLayout = () => {

    const [collapsed, setCollapsed] = useState(true);
    const [navs, setNavs] = useState(NavsForCustomer);
    const [userStatus, setUserStatus] = useState(1);

    const onCollapse = collapsed => {
        setCollapsed(collapsed)
    };

    const changeUserStatus = status => {
        if (status === userStatus) return;
        if (status === 1) {
            setUserStatus(status);
            setNavs(NavsForCustomer);
        }
        else {
            setUserStatus(status);
            setNavs(NavsForSeller);
        }
    };

    return (
        <Layout>
            <Sider collapsible collapsed={collapsed} onCollapse={onCollapse}>
                <Suspense fallback={<LoadingSuspense height="100vh" />}>
                    <AsideLeft collapsed={collapsed} navs={navs} />
                </Suspense>
            </Sider>
            <Layout>
                <NavHeader>{useToolBar(userStatus, changeUserStatus)}</NavHeader>
                <Content className="app_page">
                    <Suspense fallback={<LoadingSuspense />}>
                        <Switch>
                            {
                                Routes.map(route => <Route key={route.path} path={route.path} exact={route.exact} component={route.component} />)
                            }
                            <Redirect from={ROOT_PATH} to={DASHBOARD_PATH} />
                        </Switch>
                    </Suspense>
                </Content>
                <CustomFooter />
            </Layout>
        </Layout>
    );
}


function useToolBar(status, changeUserStatus) {
    const getButtonStyle = n =>
        status === n ? { background: "#f54469", color: "white" } : {};
    return (
        <ButtonGroup>
            <Button style={getButtonStyle(1)} onClick={() => changeUserStatus(1)}>Customer</Button>
            <Button style={getButtonStyle(0)} onClick={() => changeUserStatus(0)}>Seller</Button>
        </ButtonGroup>
    );
}


export default DefaultLayout;