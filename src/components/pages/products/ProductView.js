import React, { lazy, Suspense } from "react";
import LoadingSuspense from "../../common/LoadingSuspense";
import PageWrapper from '../../common/PageWrapper';
import { PageHeader, Button } from 'antd';
import { ADD_PRODUCT_PATH } from "../../../routes/Slugs";
import { Link } from 'react-router-dom';
const ProductList = lazy(() => import("./ProductList"));


const Products = () => {

    const pageHeader = <PageHeader title="Products" subTitle="Your product list." extra={[
        <Link to={ADD_PRODUCT_PATH}>
            <Button type="primary" icon="plus">ADD MORE PRODUCT</Button>
        </Link>
    ]}
    />
    return (
        <PageWrapper pageHeader={pageHeader}>
            <Suspense fallback={<LoadingSuspense />}>
                <ProductList />
            </Suspense>
        </PageWrapper>
    )
}

export default Products;