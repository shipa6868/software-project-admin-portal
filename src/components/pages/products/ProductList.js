import React, { useState, useEffect, useContext, Fragment } from "react";
import LoadingSuspense from "../../common/LoadingSuspense";
import { Input, Button, Icon, Switch, Table } from 'antd';
import Highlighter from 'react-highlight-words';
import { Link, Redirect } from "react-router-dom";
import { PRODUCT_INFO_PATH, EDIT_PATH } from "../../../routes/Slugs";
import { productFormData } from "../../pages/addProduct/newProductFormData";
import { ProductContext } from "../../../contexts/ProductContextProvider";
import "./Products.scss";

import { ROOT_URL } from "../../../helpers/apis";


const Products = () => {

    const { columns } = useColumnWithSearch();
    const [loading, setLoading] = useState(false);
    const [errorMsg, setErrorMsg] = useState('');
    const productContext = useContext(ProductContext);

    async function getAllProducts() {
        setLoading(true);
        const status = await productContext.getAllProducts();
        if (!status) setErrorMsg('Product Loading Error ! Please check the console or contact with PachPai');

        setLoading(false);

    }

    useEffect(() => {
        getAllProducts();
    }, [])

    return (
        <Fragment>
            {errorMsg && <h2 style={{ color: "brown" }}>{errorMsg}</h2>}
            {
                productContext.products ?
                    <Table
                        className="product_table"
                        columns={columns}
                        rowSelection={rowSelection}
                        dataSource={productContext.products.map((e, i) => ({ ...e, key: i }))}
                    /> :
                    <LoadingSuspense />
            }

        </Fragment>
    )
}

const useColumnWithSearch = () => {
    const [searchText, setSearchText] = useState('');

    const getColumnSearchProps = dataIndex => ({
        filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
            <div style={{ padding: 8 }}>
                <Input
                    placeholder={`Search ${dataIndex}`}
                    value={selectedKeys[0]}
                    onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
                    onPressEnter={() => handleSearch(selectedKeys, confirm)}
                    style={{ width: 188, marginBottom: 8, display: 'block' }}
                />
                <Button
                    type="primary"
                    onClick={() => handleSearch(selectedKeys, confirm)}
                    icon="search"
                    size="small"
                    style={{ width: 90, marginRight: 8 }}
                >
                    Search
                </Button>
                <Button onClick={() => handleReset(clearFilters)} size="small" style={{ width: 90 }}>
                    Reset
                </Button>
            </div>
        ),

        filterIcon: filtered => (
            <Icon type="search" style={{ color: filtered ? '#1890ff' : undefined }} />
        ),

        onFilter: (value, record) =>
            record[dataIndex]
                .toString()
                .toLowerCase()
                .includes(value.toLowerCase()),

        onFilterDropdownVisibleChange: visible => {
            if (visible) {
                setTimeout(() => console.log("method called"));
            }
        },

        render: text => (
            <Highlighter
                highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
                searchWords={[searchText]}
                autoEscape
                textToHighlight={text.toString()}
            />
        ),
    });

    const handleSearch = (selectedKeys, confirm) => {
        confirm();
        setSearchText(selectedKeys[0]);
    };

    const handleReset = clearFilters => {
        clearFilters();
        setSearchText('');
    };

    const columns = [
        {
            title: "Active",
            dataIndex: "active",
            key: "active",
            width: "20%",
            render: (e) => <Switch defaultChecked={e} />
        },
        {
            title: 'Image',
            dataIndex: 'thumbnailImages',
            key: 'thumbnailImages',
            width: '20%',
            render: (e) => <img src={`${ROOT_URL}${e[0]}`} style={{ height: "60px", width: "60px", borderRadius: "5px" }} alt="no content" />
        },
        {
            title: 'Name',
            dataIndex: 'name',
            key: 'name',
            width: '20%',
            ...getColumnSearchProps('name'),
        },
        {
            title: 'Action',
            dataIndex: '',
            key: 'x',
            render: (e) => (
                <Fragment>
                    <Link to={{ pathname: PRODUCT_INFO_PATH, data: e }}>
                        <Icon type="eye" />
                    </Link>
                    &nbsp;&nbsp;
                    <Link to={{ pathname: EDIT_PATH, data: {...e, description: e.description.description, videoUrl: e.description.videoUrl}, formData: productFormData, header: "Edit Product Information" }}>
                        <Icon type="edit" />
                    </Link>&nbsp;&nbsp;
                    <a href="#"><Icon type="delete" /></a>
                </Fragment>)
        },
    ];


    return { columns };
}

// rowSelection objects indicates the need for row selection
const rowSelection = {
    onChange: (selectedRowKeys, selectedRows) => {
        console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
    },
    onSelect: (record, selected, selectedRows) => {
        console.log(record, selected, selectedRows);
    },
    onSelectAll: (selected, selectedRows, changeRows) => {
        console.log(selected, selectedRows, changeRows);
    },
};


export default Products;