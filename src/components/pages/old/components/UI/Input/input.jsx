import React, { Component } from "react";
import ImageUploader from "./imageUploader";
import defaultStyle from "./input.module.css";
import Grid from "@material-ui/core/Grid";
import PackagePricing from "./packagePricingBox";
import Button from "@material-ui/core/Button";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import FormGroup from "@material-ui/core/FormGroup";
import DiscountInputBox from "./discountInputBox";

class Input extends Component {
	state = {
		packagePricingModal:
			this.props.packagePricing && this.props.packagePricing.length
				? true
				: false
	};
	render() {
		let props = this.props;
		let styles = this.props.styles ? this.props.styles : defaultStyle;
		let type = props.elementType;
		let inputClass = this.props.valid
			? styles.validInput
			: styles.validInput + " " + styles.invalidInput;
		let label = (
			<p className={styles.label}>
				{props.required && <span style={{ color: "red" }}>*</span>}
				{props.label}
			</p>
		);
		let errorMargin = props.config.value && props.config.value.length % 4;
		let errorMassage = (
			<p className={styles.errorMsg} style={{ marginLeft: errorMargin + "px" }}>
				{props.errorMsg}
			</p>
		);
		let input = "";

		switch (type) {
			case "separator":
				input = (
					<Grid container>
						<Grid item xs={12} md={12} bg={12}>
							<h3 className={styles.header}>{props.label}</h3>
						</Grid>
					</Grid>
				);
				break;
			case "input":
				input = (
					<input
						className={inputClass}
						{...this.props.config}
						onChange={e => props.changed(e.target.value, props.elementName)}
					/>
				);
				break;
			case "textarea":
				input = (
					<textarea
						className={inputClass}
						{...this.props.config}
						onChange={e => props.changed(e.target.value, props.elementName)}
						style={{ height: "200px" }}
					/>
				);
				break;
			case "checkBox":
				input = (
					<FormGroup
						className={inputClass}
						value={props.config.value}
						style={{ overflow: "auto" }}
					>
						{props.config.options.map(option => (
							<FormControlLabel
								key={option.value}
								label={option.displayValue}
								control={
									<Checkbox
										color="primary"
										value={option.value}
										checked={option.checked}
										onChange={e => props.checkboxHandle(e, props.elementName)}
									/>
								}
							/>
						))}
					</FormGroup>
				);
				break;
			case "file":
				input = (
					<ImageUploader
						className={inputClass}
						{...props.elementConfig}
						onFileLoad={props.changed}
						elementName={props.elementName}
						value={props.config.value}
					/>
				);
				break;
			case "select":
				input = (
					<select
						className={inputClass}
						onChange={e => props.changed(e.target.value, props.elementName)}
					>
						{this.props.config.options.map((e, i) => (
							<option key={i} value={e.value} style={{ color: "black" }}>
								{e.displayValue}
							</option>
						))}
					</select>
				);
				break;
			case "packagePricingBox":
				input = (
					<div>
						<Button
							variant="outlined"
							onClick={() =>
								this.setState({
									packagePricingModal: !this.state.packagePricingModal
								})
							}
							style={{ float: "left" }}
						>
							{this.state.packagePricingModal ? (
								<span>{"Close Package Pricing"}</span>
							) : (
									<span>{"Add Package Pricing"} </span>
								)}
						</Button>
						{this.state.packagePricingModal && (
							<div style={{ width: "500px" }}>
								<PackagePricing
									prices={this.props.packagePricing}
									addPriceHandle={this.props.addPriceHandle}
									deletePackagePricing={this.props.deletePackagePricing}
								/>
							</div>
						)}
					</div>
				);
				break;
			case "discountBox":
				input = (
					<div>
						<Button
							variant="outlined"
							color={this.state.discountModalOpen ? "secondary" : ""}
							onClick={() =>
								this.setState(
									{
										discountModalOpen: !this.state.discountModalOpen
									},
									() => this.props.cancelDiscount(this.state.discountModalOpen)
								)
							}
							style={{ float: "left", marginBottom: "20px" }}
						>
							{this.state.discountModalOpen ? (
								<span>CANCEL {props.label}</span>
							) : (
									<span>ADD {props.label}</span>
								)}
						</Button>

						{this.state.discountModalOpen && <DiscountInputBox props={props} />}
					</div>
				);
				break;
			case "tags":
				input = (
					<Grid container spacing={24}>

						<Grid items xs={12} sm={7} md={6} lg={8}>
							<input
								className={inputClass}
								{...props.elementConfig}
								value={props.value}
								onChange={props.changed}
								onKeyPress={props.onKeyPress}
							/>
						</Grid>
						<Grid items xs={12} sm={5} sm={12} md={3} lg={2}>
							<Button
								style={{ float: "right" }}
								variant="contained"
								component="span"
								onClick={props.buttonOnClick}
							>
								ADD TAG
              				</Button>
						</Grid>
					</Grid>
				);
				break;


			default:
				return "";
		}

		if (type === "separator") return input;
		else
			return (
				<Grid container style={{ marginBottom: "30px" }}>
					<Grid item xs={12} sm={2}>
						{label}
					</Grid>
					<Grid item xs={12} sm={10}>
						<Grid item xs={12}>
							{errorMassage}
						</Grid>
						{input}
					</Grid>
				</Grid>
			);
	}
}

export default Input;
