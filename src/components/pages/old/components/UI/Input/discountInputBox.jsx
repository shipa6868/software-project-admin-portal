import React from "react";
import classes from "./input.module.css";
import Grid from "@material-ui/core/Grid";
import DateTimeRangePicker from "./DateTimePicker";

export default function DiscountBox(props) {
	props = props.props;
	const inputClasses = [classes.validInput];
	console.log("amount", props.discount.amount);
	return (
		<Grid
			container
			style={{
				padding: "10px",
				border: "1px solid #bfbfbf"
			}}
		>
			<Grid items xs={12} sm={5} md={3} lg={2}>
				<p>AMOUNT</p>
			</Grid>
			<Grid items xs={12} sm={7} md={9} lg={10}>
				<input
					className={inputClasses.join(" ")}
					type="number"
					value={props.discount.amount}
					onChange={event => props.handleDiscountChange(event, props.index)}
				/>
				{props.discount.amount ? null : (
					<b style={{ color: "brown", float: "left" }}>
						{" "}
						Amount = 0 is not allowed !
          </b>
				)}
			</Grid>

			<Grid items xs={12} sm={5} md={3} lg={2}>
				<p>Start From</p>
			</Grid>
			<Grid items xs={12} sm={7} md={9} lg={10}>
				<DateTimeRangePicker
					label={"Starting"}
					selectedDate={props.discount.from}
					handleDateChange={value =>
						props.handleDateChange(value, "Starting", props.index)
					}
				/>
			</Grid>

			<Grid items xs={12} sm={5} md={3} lg={2}>
				<p>End At</p>
			</Grid>
			<Grid items xs={12} sm={7} md={9} lg={10}>
				<DateTimeRangePicker
					label={"Ending"}
					selectedDate={props.discount.to}
					handleDateChange={value =>
						props.handleDateChange(value, "Ending", props.index)
					}
				/>
			</Grid>
		</Grid>
	);
}
