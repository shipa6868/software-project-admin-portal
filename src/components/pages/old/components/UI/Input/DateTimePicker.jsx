import "date-fns";
import React from "react";
import Grid from "@material-ui/core/Grid";
import { makeStyles } from "@material-ui/core/styles";
import DateFnsUtils from "@date-io/date-fns";
import {
	MuiPickersUtilsProvider,
	KeyboardTimePicker,
	KeyboardDatePicker
} from "@material-ui/pickers";

const useStyles = makeStyles({
	grid: {
		width: "60%"
	}
});

export default function MaterialUIPickers(props) {
	// The first commit of Material-UI
	const classes = useStyles();

	return (
		<MuiPickersUtilsProvider utils={DateFnsUtils}>
			<Grid container className={classes.grid} justify="space-around">
				<KeyboardDatePicker
					margin="normal"
					id="mui-pickers-date"
					label={props.label + " Date"}
					value={props.selectedDate}
					onChange={event => props.handleDateChange(event, props.label)}
					KeyboardButtonProps={{
						"aria-label": "change date"
					}}
				/>
				<KeyboardTimePicker
					margin="normal"
					id="mui-pickers-time"
					label={props.label + " Time"}
					value={props.selectedDate}
					onChange={event => props.handleDateChange(event, props.label)}
					KeyboardButtonProps={{
						"aria-label": "change time"
					}}
				/>
			</Grid>
		</MuiPickersUtilsProvider>
	);
}
