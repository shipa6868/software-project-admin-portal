import React from "react";
import _ from "lodash";
import Button from "@material-ui/core/Button";
import CircularProgress from "@material-ui/core/CircularProgress";
import styles from "./imageUploader.module.scss";
// import "material-design-icons";
import Clear from "@material-ui/icons/Clear";

const onDragStyle = {
	background: "#b3d9ff",
	border: "2px dashed black"
};

const dragableStyle = {};

export default class DraggableUploader extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			loadedFiles: [...this.props.value],
			onDragStyle: dragableStyle
		};
	}

	onFileLoad = e => {
		// console.log("on File load");
		e.stopPropagation();
		const file = e.currentTarget.files[0];
		let fileReader = new FileReader();

		fileReader.onabort = () => {
			alert("Reading Aborted");
		};

		fileReader.onerror = () => {
			alert("Reading ERROR!");
		};

		if (file && file.type.match("image.*")) {
			let fileDatas = [...this.state.loadedFiles];

			let alreadyExist = false;
			fileDatas.forEach(f => {
				console.log("image file", f);
				if (f.name === file.name && f.size === file.size) {
					alert("Duplicate image not allowed !");
					alreadyExist = true;
					return;
				}
			});
			if (alreadyExist === false) {
				this.setState(
					{
						loadedFiles: [...this.state.loadedFiles, file],
						onDragStyle: dragableStyle
					},
					() =>
						this.props.onFileLoad(
							this.state.loadedFiles,
							this.props.elementName
						)
				);
			}
		}
		this.setState({ onDragStyle: dragableStyle });
	};

	addLoadedFile(file) {
		let loadedFiles = [...this.state.loadedFiles];

		loadedFiles = loadedFiles.filter(f => f.data != file.data);
		loadedFiles.push(file);
		this.setState({ loadedFiles });
		console.log(this.state.loadedFiles);
	}

	removeLoadedFile(index) {
		let loadedFiles = [...this.state.loadedFiles];
		loadedFiles = loadedFiles.filter((e, i) => i !== index);
		this.setState({ loadedFiles }, () =>
			this.props.onFileLoad(this.state.loadedFiles, this.props.elementName)
		);
	}

	updateLoadedFile(oldFile, newFile) {
		this.setState(prevState => {
			const loadedFiles = [...prevState.loadedFiles];
			_.find(loadedFiles, (file, idx) => {
				if (file == oldFile) loadedFiles[idx] = newFile;
			});

			return { loadedFiles };
		});

		return newFile;
	}

	render() {
		const { loadedFiles } = this.state;

		return (
			<div
				className={styles["inner-container"]}
				style={{
					display: "flex",
					flexDirection: "column"
				}}
			>
				<div className={styles["sub-header"]}>Drag {this.props.text}</div>
				<div
					className={styles["draggable-container"]}
					style={this.state.onDragStyle}
				>
					<input
						type="file"
						id={styles["file-browser-input"]}
						name="file-browser-input"
						ref={input => (this.fileInput = input)}
						onDragOver={e => {
							e.preventDefault();
							e.stopPropagation();
							this.setState({ onDragStyle: onDragStyle });
						}}
						onDragLeave={e => {
							this.setState({ onDragStyle: dragableStyle });
						}}
						// onDrop={this.onFileLoad.bind(this)}
						onChange={this.onFileLoad}
					/>
					<div className={styles["files-preview-container"]}>
						{loadedFiles &&
							loadedFiles.map((file, idx) => {
								return (
									<div className={styles["file"]} key={idx}>
										{typeof file === "string" ? (
											<img
												src={file}
												alt="no image"
											/>
										) : (
												<img src={URL.createObjectURL(file)} alt="no image" />
											)}
										<div className={styles["container"]}>
											<span className={styles["progress-bar"]}>
												{file.isUploading && <CircularProgress />}
											</span>
											<span
												className={styles["remove-btn"]}
												onClick={() => this.removeLoadedFile(idx)}
											>
												{/* <i className="material-icons">clear</i> */}
												<Clear />
											</span>
										</div>
									</div>
								);
							})}
					</div>

					<div className={styles["file-browser-container"]}>
						<Button
							color="primary"
							variant="outlined"
							onClick={() => this.fileInput.click()}
							onDragOver={e => {
								e.preventDefault();
								e.stopPropagation();
							}}
						>
							Browse
            </Button>
					</div>
				</div>
			</div>
		);
	}
}
