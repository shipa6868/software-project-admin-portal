import React from "react";
import Button from "@material-ui/core/Button";

export default function (props) {
	return (
		<Button
			variant={props.variant}
			color={props.color}
			className={props.className}
			style={{
				background: props.background,
				color: props.textColor
			}}
		>
			{props.children}
		</Button>
	);
}
