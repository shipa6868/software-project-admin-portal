const productForm = {
  separator1: {
    label: "PRIMARY INFO",
    elementType: "separator",

    config: {
      type: "title"
    },
    validation: {
      required: { value: false, msg: "" }
    },
    errorMsg: "",
    valid: true,
    separetor: true
  },
  name: {
    elementType: "input",
    label: "Product Name",

    config: {
      type: "text",
      placeHolder: "Product Name",
      value: ""
    },
    validation: {
      type: { value: String, msg: "name should be in alphabet." },
      maxlength: { value: 100, msg: "Maximum length 100." },
      minlength: { value: 3, msg: "Minimum length 3." },
      required: { value: true, msg: "Name is required." }
    },
    errorMsg: "",
    valid: false,
    touched: false
  },
  brandName: {
    elementType: "input",
    label: "Brand",

    config: {
      type: "text",
      placeHolder: "Brand",
      value: ""
    },
    validation: {
      type: { value: String, msg: "Brand name should be in alphabet." },
      maxlength: { value: 30, msg: "Maximum length 30." },
      minlength: { value: 2, msg: "Minimum length 2." },
      required: { value: true, msg: "Brand is required." }
    },
    errorMsg: "",
    valid: false,
    touched: false
  },

  model: {
    elementType: "input",
    label: "Model Name",

    config: {
      type: "text",
      placeHolder: "Model name",
      value: ""
    },
    validation: {
      type: { value: String, msg: "Model name should be in alphabet." },
      maxlength: { value: 100, msg: "Maximum length 100." },
      minlength: { value: 1, msg: "Minimum length 1." },
      required: { value: true, msg: "Brand is required." }
    },
    errorMsg: "",
    valid: false,
    touched: false
  },

  shortDetails: {
    elementType: "textarea",
    label: "Short Details",

    config: {
      type: "text",
      placeHolder: "Short Details",
      value: ""
    },
    validation: {
      type: { value: String, msg: "Model name should be in alphabet." },
      maxlength: { value: 100, msg: "Maximum length 100." },
      minlength: { value: 4, msg: "Minimum length 4." },
      required: { value: true, msg: "Brand is required." }
    },
    errorMsg: "",
    valid: false,
    touched: false
  },

  thumbnailImages: {
    elementType: "file",
    label: "Thumbnail Image",
    config: {
      type: "file",
      value: []
    },
    validation: {
      type: {
        value: "file",
        valid: ["jpeg", "jpg", "png"],
        msg: "Max length 100"
      },
      required: { value: true, msg: "Thumbnail image is mandatory." }
    },
    errorMsg: "",
    valid: false,
    touched: false
  },
  separator2: {
    label: "DETAIL INFORMATIONS",
    elementType: "separator",

    config: {
      type: "title"
    },
    validation: {
      required: { value: false, msg: "" }
    },
    errorMsg: "",
    valid: true,
    separetor: true
  },
  description: {
    elementType: "textarea",
    label: "Description",

    config: {
      type: "text",
      placeHolder: "Description",
      value: ""
    },
    validation: {
      type: { value: String, msg: "Description should be in alphabet." },
      maxlength: { value: 2000, msg: "Maximum length 2000." },
      minlength: { value: 10, msg: "Minimum length 10." },
      required: { value: false, msg: "Brand is required." }
    },
    errorMsg: "",
    valid: true,
    touched: false
  },
  videoUrl: {
    elementType: "input",
    label: "Video Url",

    config: {
      type: "text",
      placeHolder: "Video Url",
      value: ""
    },
    validation: {
      type: { value: String, msg: "Url should be in alphabet." },
      maxlength: { value: 2000, msg: "Maximum length 2000." },
      minlength: { value: 10, msg: "Minimum length 10." },
      required: { value: false, msg: "" }
    },
    errorMsg: "",
    valid: true,
    touched: false
  },
  addTags: {
    elementType: "tags",
    label: "Add Tags",

    config: {
      type: "text",
      placeHolder: "Add tags",
      value: ""
    },
    validation: {
      type: { value: String, msg: "tags should be in alphabet." },
      maxlength: { value: 50, msg: "Maximum length 50." },
      minlength: { value: 2, msg: "Minimum length 2." },
      required: { value: false, msg: "" }
    },
    errorMsg: "",
    valid: true,
    touched: false
  },
  separator3: {
    label: "SHIPPING INFORMATIONS",
    elementType: "separator",

    config: {
      type: "title"
    },
    validation: {
      required: { value: false, msg: "" }
    },
    errorMsg: "",
    valid: true,
    separetor: true
  },
  weight: {
    elementType: "input",
    label: "Package Weight (kg)",

    config: {
      type: "number",
      placeHolder: "Package Weight (kg)",
      value: ""
    },
    validation: {
      type: { value: Number, msg: "Weight should be number." },
      max: { value: 2000, msg: "Maximum 2000." },
      min: { value: 0.001, msg: "Minimum  0.002." },
      required: { value: true, msg: "Weight is required." }
    },
    errorMsg: "",
    valid: false,
    touched: false
  },
  height: {
    elementType: "input",
    label: "Package Height (Inch)",

    config: {
      type: "number",
      placeHolder: "Package Height (Inch)",
      value: ""
    },
    validation: {
      type: { value: Number, msg: "Height should be number." },
      max: { value: 2000, msg: "Maximum 2000." },
      min: { value: 0.001, msg: "Minimum  0.002." },
      required: { value: true, msg: "Height is required." }
    },
    errorMsg: "",
    valid: false,
    touched: false
  },
  width: {
    elementType: "input",
    label: "Package Width (Inch)",

    config: {
      type: "number",
      placeHolder: "Package Width (Inch)",
      value: ""
    },
    validation: {
      type: { value: Number, msg: "Width should be number." },
      max: { value: 2000, msg: "Maximum 2000." },
      min: { value: 0.001, msg: "Minimum  0.002." },
      required: { value: true, msg: "Width is required." }
    },
    errorMsg: "",
    valid: false,
    touched: false
  },
  length: {
    elementType: "input",
    label: "Package Length (Inch)",

    config: {
      type: "number",
      placeHolder: "Package Length (Inch)",
      value: ""
    },
    validation: {
      type: { value: Number, msg: "Length should be number." },
      max: { value: 2000, msg: "Maximum 2000." },
      min: { value: 0.001, msg: "Minimum  0.002." },
      required: { value: true, msg: "Length is required." }
    },
    errorMsg: "",
    valid: false,
    touched: false
  },
  warehouseAddresses: {
    elementType: "checkBox",
    label: "Warehouse Addresses",

    config: {
      type: "select",
      options: [
        {
          value: "khilkhet",
          displayValue: "Khilkhet",
          checked: false
        },
        {
          value: "uttara",
          displayValue: "Malibag",
          checked: false
        },
        {
          value: "mohammad pur",
          displayValue: "Mohammad pur",
          checked: false
        }
      ],
      value: []
    },
    validation: {
      type: { value: Array, msg: "" },
      required: { value: false, msg: "" }
    },
    errorMsg: "",
    valid: true,
    touched: false
  }
};

export default productForm;
