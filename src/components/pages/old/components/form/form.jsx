import React, { useState, useEffect } from "react";
import Button from "@material-ui/core/Button";
import _ from "lodash";
import Input from "../UI/Input/input";
import axios from "axios";
import styles from "./form.module.css";
import inputStyle from "./inputStyle.module.css"
import CircularProgress from '@material-ui/core/CircularProgress';


export default function (props) {

	const [formData, setFormData] = useState(createForm(props.formData, props.data));
	const [isValid, setIsValid] = useState(false);
	const [loading, setLoading] = useState(false);
	const [errorMassage, setErrorMassage] = useState(null);
	const inputChangeHandler = (value, elementName) => {
		let newFormData = JSON.parse(JSON.stringify(formData));
		if (elementName === "newPasswordAgain") {
			newFormData[elementName].config.value = value;
			if (value !== newFormData.newPassword.config.value) {
				newFormData[elementName].valid = false;
				newFormData[elementName].errorMsg = " Did not match with new password.";
				newFormData[elementName].touched = true;
			}
			else {
				newFormData[elementName].valid = true;
				newFormData[elementName].errorMsg = "";
			}
			setFormData(newFormData);
			setIsValid(formIsValid(newFormData));
			return;
		}

		newFormData[elementName].config.value = value;

		let validity = checkValidity(value, newFormData[elementName].validation);
		newFormData[elementName].valid = validity.valid;
		newFormData[elementName].errorMsg = validity.errorMsg;
		newFormData[elementName].touched = true;
		setFormData(newFormData);
		setIsValid(formIsValid(newFormData));
	}

	const customerSubmitHandle = () => {
		setLoading(true);
		let data = {};
		// console.log(props.data._id)
		for (let element in formData) {
			if (formData[element].separetor === undefined && element !== "newPasswordAgain" && formData[element].config.value)
				data[element] = formData[element].config.value;
		}
		if (props.data)
			data._id = props.data._id;
		axios
			.put(props.url, data, {
				headers: {
					"Content-Type": "application/json",
					"x-auth-token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1ZDk4OGU3ZmE3ZGUzMDE4ZTkxYTg3OTkiLCJpYXQiOjE1NzAyNzkwNTB9.m8DWkGVJRO-BHCFBEEShx7WPTQCIXgaNEpgJBo7ZwkQ"
				}
			})
			.then(response => {
				setLoading(false);
				setIsValid(false);
				window.alert("Done..!")
			})
			.catch(error => {
				setLoading(false);
				setIsValid(false);
				setErrorMassage(error.response.data);

			});

	};

	return (
		<div className={styles.body}>
			{!loading ?
				<div>
					{
						createFormData(formData).map((e, i) => (
							<Input
								key={i}
								elementType={e.elementType}
								config={e.config}
								label={e.label}
								valid={e.valid}
								errorMsg={e.errorMsg}
								required={e.required}
								elementName={e.elementName}
								changed={inputChangeHandler}
								styles={inputStyle}
							/>
						))
					}{
						errorMassage && <h4 className={styles.errorMassage}>{errorMassage}</h4>
					}
					<Button disabled={!isValid}
						variant="contained" color="primary"
						onClick={customerSubmitHandle}
					>
						Update
          </Button>
				</div> : <Progress />
			}
		</div>
	)
}

const createForm = (formData, data) => {
	if (data)
		for (let e in formData) {
			formData[e].config.value = data[e] ? data[e] : formData[e].config.value;
		}
	return formData;
}


const createFormData = data => {
	let elementArray = [];
	for (let element in data) {
		let formElement = {
			label: data[element].label,
			elementType: data[element].elementType,
			config: data[element].config,
			valid: data[element].touched ? data[element].valid : true,
			errorMsg: data[element].errorMsg,
			required: data[element].validation.required.value,
			elementName: element
		};
		elementArray.push(formElement);
	}
	return elementArray;
};


const checkValidity = (value, rules) => {
	if (rules === undefined) {
		return true;
	}

	let isValid = true;
	// if (!rules) {
	//   return true;
	// }.
	let errorMsg = "";

	if (
		rules.required.value &&
		rules.type.value !== "file" &&
		rules.type.value !== "array"
	) {
		isValid = value.trim() !== "" && isValid;
		errorMsg += isValid ? "" : rules.required.msg;
		if (!isValid) return { valid: isValid, errorMsg: errorMsg };
	}

	if (rules.type.value === "file" && rules.required.value) {
		isValid = value !== null;
		errorMsg += isValid ? "" : rules.required.msg;
		// return { valid: isValid, errorMsg: errorMsg };
	}

	if (rules.minlength) {
		isValid = value.length >= rules.minlength.value && isValid;
		errorMsg +=
			value.length < rules.minlength.value ? rules.minlength.msg + "" : "";
	}

	if (rules.maxlength) {
		isValid = value.length <= rules.maxlength.value && isValid;
		errorMsg +=
			value.length > rules.maxlength.value ? rules.maxlength.msg + "" : "";
	}

	if (rules.type.value === Number) {
		// const pattern = /^\d+$/;
		// isValid = pattern.test(value) && isValid;
		isValid = value !== NaN;
		errorMsg += isValid ? "" : rules.type.msg;
		if (isValid) {
			isValid = value >= rules.min.value && value <= rules.max.value;
			errorMsg += value >= rules.min.value ? "" : rules.min.msg;
			errorMsg += value <= rules.max.value ? "" : rules.max.msg;
		}
	}
	//console.log(rules);
	if (rules.type.value === Array && rules.required.value) {
		isValid = value.length > 0 && isValid;
	}

	return { valid: isValid, errorMsg: errorMsg };
};

const formIsValid = form => {
	let isValid = true;

	for (let e in form) {
		if (form[e].separetor === undefined) {
			if (form[e].valid === false) {
				isValid = false;
				break;
			}
		}
	}

	return isValid;
};


const Progress = () => {

	const [progress, setProgress] = React.useState(0);

	React.useEffect(() => {
		function tick() {
			// reset when reaching 100%
			setProgress(oldProgress => (oldProgress >= 100 ? 0 : oldProgress + 1));
		}

		const timer = setInterval(tick, 20);
		return () => {
			clearInterval(timer);
		};
	}, []);

	return (
		<CircularProgress variant="determinate" value={progress} />
	);

}
