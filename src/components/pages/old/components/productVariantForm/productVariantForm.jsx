import React, { Component } from "react";
import Input from "../UI/Input/input";
import Button from "@material-ui/core/Button";
import classes from "../productForm/productForm.module.css";
import Spinner from "../UI/Spinner/Spinner";
import productVariantForm from "./data";
import VariantTable from "./variantTable";
import _ from "lodash";

const initialDiscount = {
	amount: 0,
	from: new Date(),
	to: new Date()
};

const getDiscountAmount = variantArray => {
	let discount = { ...initialDiscount };
	if (variantArray.length) {
		if (variantArray[0].discountAmount) {
			discount = variantArray[0].discountAmount;
		}
	}
	return discount;
};

const getDiscountPercentage = variantArray => {
	let discount = { ...initialDiscount };
	if (variantArray.length) {
		if (variantArray[0].discountPercentage) {
			discount = variantArray[0].discountPercentage;
		}
	}
	return discount;
};

class productVariantAddForm extends Component {
	state = {
		productVariantForm: productVariantForm,
		productVariantArray: [...this.props.productVariantArray],
		variantFormIsValid: false,
		loading: false,
		percentageWarning: "",
		amountWarning: "",
		discountAmount: getDiscountAmount(this.props.productVariantArray),
		discountPercentage: getDiscountPercentage(this.props.productVariantArray),
		packagePricing: []
	};

	componentWillMount() {
		if (this.props.updating) {
			let variant = this.props.productVariantArray[0];
			let productVariantForm = { ...this.state.productVariantForm };
			productVariantForm.colorFamily.value = variant.attrs.colorFamily;
			productVariantForm.colorFamily.valid = true;

			productVariantForm.size.value = variant.attrs.size;
			productVariantForm.size.valid = true;

			productVariantForm.price.value = variant.price;
			productVariantForm.price.valid = true;

			productVariantForm.quantity.value = variant.quantity;
			productVariantForm.quantity.valid = true;

			productVariantForm.images.value = variant.images;
			productVariantForm.images.valid = true;

			if (variant.packagePricing)
				this.setState({ packagePricing: variant.packagePricing });
		}
	}

	productVariantHandler = event => {
		event.preventDefault();
		const productVariantArray = [...this.state.productVariantArray];
		let alreadyExists = false;
		productVariantArray.forEach(variant => {
			if (
				this.state.productVariantForm.colorFamily.config.value ===
				variant.colorFamily &&
				this.state.productVariantForm.size.config.value === variant.size
			) {
				alert("Duplicate product variant not allowed");
				alreadyExists = true;
				return;
			}
		});

		if (alreadyExists) return;

		let productVariant = {};
		for (let formElementIdentifier in this.state.productVariantForm) {
			if (
				this.state.productVariantForm[formElementIdentifier].elementType !==
				"separator"
			)
				productVariant[formElementIdentifier] = this.state.productVariantForm[
					formElementIdentifier
				].config.value;
		}

		productVariant.discountAmount = this.state.discountAmount;
		productVariant.discountPercentage = this.state.discountPercentage;

		productVariant.isAvailable = true;

		let specialPrice = [...this.state.packagePricing];
		productVariant.specialPrice = specialPrice;

		productVariantArray.push(productVariant);
		this.setState({ productVariantArray }, () => {
			this.props.updating
				? this.props.updateVariantHandler(productVariant)
				: this.props.addVariantHandler(this.state.productVariantArray);
		});
	};

	deletePackagePricing = (serial, index) => {
		if (serial === undefined) {
			let packagePricing = [...this.state.packagePricing];
			packagePricing = packagePricing.filter((e, i) => i != index);
			this.setState({ packagePricing });
		} else {
			let productVariantArray = [...this.state.productVariantArray];
			let productVariant = { ...productVariantArray[serial] };
			productVariant.specialPrice = [
				...productVariant.specialPrice.filter((e, i) => i != index)
			];
			productVariantArray[serial] = productVariant;
			this.setState({ productVariantArray });
		}
	};

	handleDiscountAmountDateRange = (value, label, index) => {
		let discountAmount = { ...this.state.discountAmount };
		if (label === "Starting") {
			if (index !== undefined) {
				let productVariantArray = [...this.state.productVariantArray];
				let productVariant = { ...productVariantArray[index] };
				productVariant.discountAmount.from = value;
				productVariantArray[index] = productVariant;
				this.setState({ productVariantArray }, () =>
					this.props.addVariantHandler(this.state.productVariantArray)
				);
			} else {
				discountAmount.from = value;
				this.setState({ discountAmount });
			}
		} else {
			if (index !== undefined) {
				let productVariantArray = [...this.state.productVariantArray];
				let productVariant = { ...productVariantArray[index] };
				productVariant.discountAmount.to = value;
				productVariantArray[index] = productVariant;
				this.setState({ productVariantArray }, () =>
					this.props.addVariantHandler(this.state.productVariantArray)
				);
			} else {
				discountAmount.to = value;
				this.setState({ discountAmount });
			}
		}
	};

	handleDiscountPercentageDateRange = (value, label, index) => {
		let discountPercentage = { ...this.state.discountPercentage };
		if (label === "Starting") {
			if (index !== undefined) {
				let productVariantArray = [...this.state.productVariantArray];
				let productVariant = { ...productVariantArray[index] };
				productVariant.discountPercentage.from = value;
				productVariantArray[index] = productVariant;
				this.setState({ productVariantArray }, () =>
					this.props.addVariantHandler(this.state.productVariantArray)
				);
			} else {
				discountPercentage.from = value;
				this.setState({ discountPercentage });
			}
		} else {
			if (index !== undefined) {
				let productVariantArray = [...this.state.productVariantArray];
				let productVariant = { ...productVariantArray[index] };
				productVariant.discountPercentage.to = value;
				productVariantArray[index] = productVariant;
				this.setState({ productVariantArray }, () =>
					this.props.addVariantHandler(this.state.productVariantArray)
				);
			} else {
				discountPercentage.to = value;
				this.setState({ discountPercentage });
			}
		}
	};

	handleDiscountPercentage = (event, index) => {
		if (event.target.value < 1) return;
		if (index !== undefined) {
			let productVariantArray = [...this.state.productVariantArray];
			let productVariant = { ...productVariantArray[index] };
			productVariant.discountPercentage.amount = event.target.value;
			productVariantArray[index] = productVariant;
			this.setState({ productVariantArray }, () =>
				this.props.addVariantHandler(this.state.productVariantArray)
			);
		} else {
			let discountPercentage = { ...this.state.discountPercentage };
			discountPercentage.amount = event.target.value;
			this.setState({ discountPercentage });
		}
	};

	handleDiscountAmount = (event, index) => {
		if (event.target.value < 1) return;
		if (index !== undefined) {
			let productVariantArray = [...this.state.productVariantArray];
			let productVariant = { ...productVariantArray[index] };
			productVariant.discountAmount.amount = event.target.value;
			productVariantArray[index] = productVariant;
			this.setState({ productVariantArray }, () =>
				this.props.addVariantHandler(this.state.productVariantArray)
			);
		} else {
			let discountAmount = { ...this.state.discountAmount };
			discountAmount.amount = event.target.value;
			this.setState({ discountAmount });
		}
	};

	cancelDiscountPercentage = value => {
		!value && this.setState({ discountPercentage: initialDiscount });
	};
	cancelDiscountAmount = value => {
		!value && this.setState({ discountAmount: initialDiscount });
	};

	deleteButtonAction = (event, index) => {
		let productVariantArray = [...this.state.productVariantArray];
		productVariantArray = productVariantArray.filter((e, i) => i != index);
		this.setState({ productVariantArray }, () =>
			this.props.addVariantHandler(this.state.productVariantArray)
		);
	};

	onChangeAvailability = (event, index) => {
		// let value = event.target.value;
		let productVariantArray = [...this.state.productVariantArray];
		productVariantArray = productVariantArray.map((variant, i) => {
			if (i == index) {
				variant.isAvailable = !variant.isAvailable;
			}
			return variant;
		});
		this.setState({ productVariantArray });
	};

	variantInputChangeHandle = (event, index, element) => {
		let value = event.target.value;
		let productVariantArray = [...this.state.productVariantArray];
		productVariantArray = productVariantArray.map((variant, i) => {
			if (i == index) {
				variant[element] = value >= 0 ? value : 0;
			}
			return variant;
		});
		this.setState({ productVariantArray }, () =>
			this.props.addVariantHandler(this.state.productVariantArray)
		);
	};

	duplicatePrice = (piece, price, arr) => {
		let duplicate = false;
		arr.forEach(e => {
			if (e.piece === piece && e.price === price) {
				alert("Duplicate not allowed !");
				duplicate = true;
				return;
			}
		});
		return duplicate;
	};

	addPriceHandle = (piece, price, index) => {
		if (piece <= 1 || price <= 0) return;
		if (index === undefined) {
			let packagePricing = [...this.state.packagePricing];
			//check for duplicate
			if (this.duplicatePrice(piece, price, packagePricing)) return;
			packagePricing.push({ piece, price });
			this.setState({ packagePricing });
		} else {
			let specialPrice = { piece: piece, price: price };
			let productVariantArray = [...this.state.productVariantArray];
			productVariantArray = productVariantArray.map((e, i) => {
				if (i === index) {
					if (!this.duplicatePrice(piece, price, e.specialPrice)) {
						e.specialPrice.push(specialPrice);
					}
				}
				return e;
			});
			this.setState({ productVariantArray }, () =>
				this.props.addVariantHandler(this.state.productVariantArray)
			);
		}
	};

	inputChangedHandler = (value, inputIdentifier) => {
		const updatedProductVariantForm = {
			...this.state.productVariantForm
		};
		const updatedFormElement = {
			...updatedProductVariantForm[inputIdentifier]
		};

		updatedFormElement.config.value = value;

		let validity = checkValidity(value, updatedFormElement.validation);
		updatedFormElement.valid = validity.valid;
		updatedFormElement.errorMsg = validity.errorMsg;

		updatedFormElement.touched = true;
		updatedProductVariantForm[inputIdentifier] = updatedFormElement;

		this.setState({
			productVariantForm: updatedProductVariantForm
		});
		this.props.checkVariantFormIsValid(formIsValid(updatedProductVariantForm));
	};

	checkboxHandle = (event, inputIdentifier) => {
		const updatedProductVariantForm = {
			...this.state.productVariantForm
		};
		const updatedFormElement = {
			...updatedProductVariantForm[inputIdentifier]
		};

		let options = [...updatedFormElement.config.options];
		options = options.map(option => {
			if (option.value == event.target.value) {
				option.checked = event.target.checked;
			}
			return option;
		});
		updatedFormElement.config.options = options;
		let values = updatedFormElement.config.value;
		if (event.target.checked) values.push(event.target.value);
		else values = values.filter(value => value != event.target.value);
		values = [...new Set(values)];
		updatedFormElement.config.value = values;
		updatedFormElement.touched = true;
		let validity = this.checkValidity(
			updatedFormElement.value,
			updatedFormElement.validation
		);
		updatedFormElement.valid = validity.valid;
		updatedFormElement.errorMsg = validity.errorMsg;
		updatedProductVariantForm[inputIdentifier] = updatedFormElement;

		this.setState({
			productVariantForm: updatedProductVariantForm
		});
		this.props.checkVariantFormIsValid(formIsValid(updatedProductVariantForm));
	};

	addVariantInputChangedHandler = (event, inputIdentifier) => {
		const updatedProductForm = {
			...this.state.productVariantForm
		};
		const updatedFormElement = {
			...updatedProductForm[inputIdentifier]
		};
		if (inputIdentifier == "images") {
			updatedFormElement.value = event.target.files[0];
		} else updatedFormElement.value = event.target.value;
		updatedFormElement.valid = checkValidity(
			updatedFormElement.value,
			updatedFormElement.validation
		);

		updatedFormElement.touched = true;
		updatedProductForm[inputIdentifier] = updatedFormElement;

		let formIsValid = true;
		for (let inputIdentifier in updatedProductForm) {
			formIsValid = updatedProductForm[inputIdentifier].valid && formIsValid;
		}
		this.setState({
			productVariantForm: updatedProductForm,
			variantFormIsValid: formIsValid
		});
	};

	render() {
		const formElementsArray = createFormData(this.state.productVariantForm);
		let form = (
			<form>
				<div className={classes.formHeader}>
					<h3 style={{ textAlign: "left" }}>ADD VARIANTS</h3>
					<hr />
				</div>

				{formElementsArray.map((formElement, i) => {
					return (
						<Input
							key={i}
							elementType={formElement.elementType}
							config={formElement.config}
							label={formElement.label}
							valid={formElement.valid}
							errorMsg={formElement.errorMsg}
							required={formElement.required}
							elementName={formElement.elementName}
							addDiscountInfo={this.addDiscountInfo}
							discount={
								formElement.label === "Discount Percentage"
									? this.state.discountPercentage
									: this.state.discountAmount
							}
							handleDateChange={
								formElement.label === "Discount Percentage"
									? this.handleDiscountPercentageDateRange
									: this.handleDiscountAmountDateRange
							}
							handleDiscountChange={
								formElement.label === "Discount Percentage"
									? this.handleDiscountPercentage
									: this.handleDiscountAmount
							}
							cancelDiscount={
								formElement.label === "Discount Percentage"
									? this.cancelDiscountPercentage
									: this.cancelDiscountAmount
							}
							packagePricing={this.state.packagePricing}
							addPriceHandle={this.addPriceHandle}
							deletePackagePricing={this.deletePackagePricing}
							changed={this.inputChangedHandler}
							checkboxHandle={this.checkboxHandle}
						/>
					);
				})}
				<Button
					style={{ left: "0px", display: "block", marginBottom: "50px" }}
					color="primary"
					disabled={!this.props.variantFormIsValid}
					variant="contained"
					onClick={e => this.productVariantHandler(e)}
				>
					{this.props.updating
						? "UPDATE VARIANT"
						: this.state.productVariantArray.length === 0
							? "ADD VARIANT"
							: "ADD MORE VARIANT"}
				</Button>

				{this.state.productVariantArray.length > 0 && !this.props.updating && (
					<VariantTable
						variants={this.state.productVariantArray}
						variantInputChangeHandle={this.variantInputChangeHandle}
						onChangeAvailability={this.onChangeAvailability}
						deleteButtonAction={this.deleteButtonAction}
						addPriceHandle={this.addPriceHandle}
						amountWarning={this.state.amountWarning}
						percentageWarning={this.state.percentageWarning}
						addDiscountInfo={this.addDiscountInfo}
						discountPercentage={this.state.discountPercentage}
						discountAmount={this.state.discountAmount}
						handleDiscountPercentageDateRange={
							this.handleDiscountPercentageDateRange
						}
						handleDiscountAmountDateRange={this.handleDiscountAmountDateRange}
						handleDiscountPercentage={this.handleDiscountPercentage}
						handleDiscountAmount={this.handleDiscountAmount}
						cancelDiscountPercentage={this.cancelDiscountPercentage}
						cancelDiscountAmount={this.cancelDiscountAmount}
						deletePackagePricing={this.deletePackagePricing}
					/>
				)}

				<hr />
				{this.props.isUpdating && (
					<Button
						style={{ marginTop: "30px" }}
						color="primary"
						disabled={!this.state.productVariantArray.length}
						variant="contained"
						onClick={this.productHandler}
					>
						Save Variants
          </Button>
				)}
			</form>
		);
		if (this.state.loading) {
			form = <Spinner />;
		}
		return <div className={classes.ProductData}>{form}</div>;
	}
}

const createFormData = data => {
	let elementArray = [];
	for (let element in data) {
		let formElement = {
			label: data[element].label,
			elementType: data[element].elementType,
			config: data[element].config,
			valid: data[element].touched ? data[element].valid : true,
			errorMsg: data[element].errorMsg,
			required: data[element].validation.required.value,
			elementName: element
		};
		elementArray.push(formElement);
	}
	return elementArray;
};

const checkValidity = (value, rules) => {
	if (rules === undefined) {
		return true;
	}

	let isValid = true;
	// if (!rules) {
	//   return true;
	// }.
	let errorMsg = "";

	if (
		rules.required.value &&
		rules.type.value !== "file" &&
		rules.type.value !== "array"
	) {
		isValid = value.trim() !== "" && isValid;
		errorMsg += isValid ? "" : rules.required.msg;
		if (!isValid) return { valid: isValid, errorMsg: errorMsg };
	}

	if (rules.type.value === "file" && rules.required.value) {
		isValid = value !== null;
		errorMsg += isValid ? "" : rules.required.msg;
		return { valid: isValid, errorMsg: errorMsg };
	}

	if (rules.minlength) {
		isValid = value.length >= rules.minlength.value && isValid;
		errorMsg +=
			value.length < rules.minlength.value ? rules.minlength.msg + "" : "";
	}

	if (rules.maxlength) {
		isValid = value.length <= rules.maxlength.value && isValid;
		errorMsg +=
			value.length > rules.maxlength.value ? rules.maxlength.msg + "" : "";
	}

	if (rules.type.value === Number) {
		// const pattern = /^\d+$/;
		// isValid = pattern.test(value) && isValid;
		isValid = !isNaN(value);
		errorMsg += isValid ? "" : rules.type.msg;
		if (isValid) {
			isValid = value >= rules.min.value && value <= rules.max.value;
			errorMsg += value >= rules.min.value ? "" : rules.min.msg;
			errorMsg += value <= rules.max.value ? "" : rules.max.msg;
		}
	}
	//console.log(rules);
	if (rules.type.value === Array) {
		isValid = value.length > 0 && isValid;
	}

	return { valid: isValid, errorMsg: errorMsg };
};

const formIsValid = form => {
	let isValid = true;

	for (let e in form) {
		if (form[e].separetor === undefined) {
			if (form[e].valid === false) {
				isValid = false;
				break;
			}
		}
	}

	return isValid;
};

export default productVariantAddForm;
