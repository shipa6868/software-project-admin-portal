import React, { Component } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import Modal from "@material-ui/core/Modal";
import Button from "@material-ui/core/Button";
import Switch from "@material-ui/core/Switch";
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from "@material-ui/icons/Delete";
import Grid from "@material-ui/core/Grid";
import PackagePricingBox from "../UI/Input/packagePricingBox";
import DiscountBox from "../UI/Input/discountInputBox";

const root = {
	width: "100%",

	overflowX: "auto"
};
const table = {
	minWidth: 650
};

const tableCellStyle = {
	fontSize: "16px",
	height: "100px"
};

const inputStyle = {
	height: "30px",
	width: "100px",
	padding: "5px",
	fontSize: "16px"
};

const specialPriceStyle = {
	height: "100px",
	overflow: "auto",
	cursor: "pointer"
};

const modalStyle = () => {
	const top = 20;
	const left = 20;

	return {
		textAlign: "center",
		top: `${top}%`,
		left: `${left}%`,
		transform: `translate(-${top}%, -${left}%)`,
		position: "absolute",
		width: 700,
		height: "500px",
		overflow: "auto",
		backgroundColor: "white",
		boxShadow: "0px 0px 61px -11px black",
		outline: "none",
		fontFamily: "Arial",
		color: "#3c3c3c",
		paddingBottom: "30px"
	};
};

function setStateValue(array, t) {
	let arr = [];
	array.forEach(e => arr.push(t));
	return arr;
}

class SimpleTable extends Component {
	state = {
		percentageModalOpen: setStateValue(this.props.variants, false),
		amountModalOpen: setStateValue(this.props.variants, false),
		priceModalOpen: setStateValue(this.props.variants, false)
	};

	handlePercentageModalOpen = i => {
		let percentageModalOpen = [...this.state.percentageModalOpen];
		percentageModalOpen[i] = true;
		this.setState({ percentageModalOpen });
	};

	handleAmountModalOpen = i => {
		let amountModalOpen = [...this.state.amountModalOpen];
		amountModalOpen[i] = true;
		this.setState({ amountModalOpen });
	};

	handlePriceModalOpen = i => {
		let priceModalOpen = [...this.state.priceModalOpen];
		priceModalOpen[i] = true;
		this.setState({ priceModalOpen });
	};
	handlePriceModalClose = i => {
		let priceModalOpen = [...this.state.priceModalOpen];
		priceModalOpen[i] = false;
		this.setState({ priceModalOpen });
	};

	closePercentageModal = i => {
		let percentageModalOpen = [...this.state.percentageModalOpen];
		percentageModalOpen[i] = false;
		this.setState({ percentageModalOpen });
	};

	closeAmountModal = i => {
		let amountModalOpen = [...this.state.amountModalOpen];
		amountModalOpen[i] = false;
		this.setState({ amountModalOpen });
	};

	render() {
		const props = this.props;
		return (
			<Paper style={root}>
				<Table style={table}>
					<TableHead>
						<TableRow>
							<TableCell> Available</TableCell>
							<TableCell>Image</TableCell>
							<TableCell align="right">Color Family</TableCell>
							<TableCell align="right">Size</TableCell>
							<TableCell align="right">Quantity</TableCell>
							<TableCell align="right">Price</TableCell>
							<TableCell align="right">Discount (%)</TableCell>
							<TableCell align="right">Discount (A)</TableCell>
							<TableCell align="right">Special Price</TableCell>
							<TableCell align="right">Delete</TableCell>
						</TableRow>
					</TableHead>
					<TableBody>
						{props.variants.length &&
							props.variants.map((row, i) => {
								return (
									<TableRow key={i}>
										<TableCell>
											<Switch
												checked={row.isAvailable}
												onChange={e => props.onChangeAvailability(e, i)}
												value="checkedB"
												color="primary"
											/>
										</TableCell>
										<TableCell>
											{typeof row.images[0] === "string" ? (
												<img
													src={"http://localhost:3002" + row.images[0]}
													alt="no image"
													style={{ height: "80px", width: "80px" }}
												/>
											) : (
													<img
														src={URL.createObjectURL(row.images[0])}
														style={{ height: "80px", width: "80px" }}
													/>
												)}
										</TableCell>
										<TableCell align="right" style={tableCellStyle}>
											{row.colorFamily}
										</TableCell>
										<TableCell align="right" style={tableCellStyle}>
											{row.size}
										</TableCell>
										<TableCell align="right" style={tableCellStyle}>
											<input
												type="number"
												style={inputStyle}
												value={row.quantity}
												onChange={e =>
													props.variantInputChangeHandle(e, i, "quantity")
												}
											/>
										</TableCell>
										<TableCell align="right" style={tableCellStyle}>
											<input
												style={inputStyle}
												type="number"
												value={row.price}
												onChange={e =>
													props.variantInputChangeHandle(e, i, "price")
												}
											/>
										</TableCell>
										<TableCell align="right" style={tableCellStyle}>
											<Button
												onClick={() => this.handlePercentageModalOpen(i)}
												variant="outlined"
												style={{
													cursor: "pointer"
												}}
											>
												View
                      </Button>

											<Modal
												aria-labelledby="simple-modal-title"
												aria-describedby="simple-modal-description"
												open={this.state.percentageModalOpen[i]}
												onClose={() => this.closePercentageModal(i)}
											>
												<div style={modalStyle()}>
													<h3 style={{ background: "whitesmoke" }}>
														Update Discount Percentage
                          </h3>
													<DiscountBox
														props={{
															discount: {
																amount: row.discountPercentage.amount,
																from: row.discountPercentage.from,
																to: row.discountPercentage.to
															},
															handleDiscountChange: this.props
																.handleDiscountPercentage,

															handleDateChange: this.props
																.handleDiscountPercentageDateRange,

															index: i
														}}
													/>
													<Button
														variant="contained"
														color="primary"
														onClick={() => this.closePercentageModal(i)}
														style={{ marginTop: "30px" }}
													>
														DONE
                          </Button>
												</div>
											</Modal>
										</TableCell>
										<TableCell align="right" style={tableCellStyle}>
											<Button
												onClick={() => this.handleAmountModalOpen(i)}
												variant="outlined"
												style={{
													cursor: "pointer"
												}}
											>
												View
                      </Button>

											<Modal
												aria-labelledby="simple-modal-title"
												aria-describedby="simple-modal-description"
												open={this.state.amountModalOpen[i]}
												onClose={() => this.closeAmountModal(i)}
											>
												<div style={modalStyle()}>
													<h3 style={{ background: "whitesmoke" }}>
														Update Discount Amount
                          </h3>
													<DiscountBox
														props={{
															discount: {
																amount: row.discountAmount.amount,
																from: row.discountAmount.from,
																to: row.discountAmount.to
															},
															handleDiscountChange: this.props
																.handleDiscountAmount,

															handleDateChange: this.props
																.handleDiscountAmountDateRange,

															index: i
														}}
													/>
													<Button
														variant="contained"
														color="primary"
														onClick={() => this.closeAmountModal(i)}
														style={{ marginTop: "30px" }}
													>
														DONE
                          </Button>
												</div>
											</Modal>
										</TableCell>

										<TableCell align="right" style={tableCellStyle}>
											{row.specialPrice.length ? (
												<div
													style={specialPriceStyle}
													onClick={() => this.handlePriceModalOpen(i)}
												>
													{row.specialPrice.map((e, i) => (
														<p key={i}>
															{e.piece} piece = {e.price} BDT
                            </p>
													))}
												</div>
											) : (
													<Button
														variant="outlined"
														onClick={() => this.handlePriceModalOpen(i)}
													>
														ADD
                        </Button>
												)}

											<Modal
												aria-labelledby="simple-modal-title"
												aria-describedby="simple-modal-description"
												open={this.state.priceModalOpen[i]}
												onClose={() => this.handlePriceModalClose(i)}
											>
												<div style={modalStyle()}>
													<PackagePricingBox
														prices={row.specialPrice}
														index={i}
														addPriceHandle={props.addPriceHandle}
														deletePackagePricing={props.deletePackagePricing}
													/>
												</div>
											</Modal>
										</TableCell>
										<TableCell align="right" style={tableCellStyle}>
											<IconButton
												aria-label="Delete"
												color="secondary"
												onClick={e => props.deleteButtonAction(e, i)}
											>
												<DeleteIcon style={{ color: "#e94f34" }} />
											</IconButton>
										</TableCell>
									</TableRow>
								);
							})}
					</TableBody>
				</Table>
			</Paper>
		);
	}
}

export default SimpleTable;
