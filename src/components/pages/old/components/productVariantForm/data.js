export default {
	colorFamily: {
		elementType: "select",
		label: "Color Family",

		config: {
			type: "select",
			options: [
				{ value: "", displayValue: "Choose a color family" },
				{ value: "green", displayValue: "Green" },
				{ value: "blue", displayValue: "Blue" },
				{ value: "black", displayValue: "Black" },
				{ value: "white", displayValue: "White" }
			],
			value: ""
		},
		validation: {
			type: { value: String, msg: "Color family should be in alphabet." },
			required: { value: true, msg: "Color family is required." }
		},
		errorMsg: "",
		valid: false,
		touched: false
	},
	size: {
		elementType: "select",
		label: "Size",

		config: {
			type: "select",
			options: [
				{ value: "", displayValue: "Choose a size" },
				{ value: "xs", displayValue: "XS" },
				{ value: "sm", displayValue: "SM" },
				{ value: "md", displayValue: "MD" },
				{ value: "lg", displayValue: "LG" },
				{ value: "xl", displayValue: "XL" },
				{ value: "xxl", displayValue: "XXL" },
				{ value: "22", displayValue: "22" },
				{ value: "24", displayValue: "24" },
				{ value: "28", displayValue: "28" },
				{ value: "29", displayValue: "29" },
				{ value: "30", displayValue: "30" },
				{ value: "31", displayValue: "31" },
				{ value: "32", displayValue: "32" },
				{ value: "33", displayValue: "33" },
				{ value: "34", displayValue: "34" },
				{ value: "35", displayValue: "35" },
				{ value: "36", displayValue: "36" }
			],
			value: ""
		},
		validation: {
			type: { value: String, msg: "Size should be in alphabet." },
			required: { value: true, msg: "Size family is required." }
		},
		errorMsg: "",
		valid: false,
		touched: false
	},
	price: {
		elementType: "input",
		label: "Price",

		config: {
			type: "number",
			placeHolder: "Price",
			value: ""
		},
		validation: {
			type: { value: Number, msg: "Price should be number." },
			max: { value: 99999999, msg: "Maximum 99999999." },
			min: { value: 0, msg: "Minimum  0." },
			required: { value: true, msg: "Price is required." }
		},
		errorMsg: "",
		valid: false,
		touched: false
	},
	quantity: {
		elementType: "input",
		label: "Quantity",

		config: {
			type: "number",
			placeHolder: "Quantity",
			value: ""
		},
		validation: {
			type: { value: Number, msg: "Quantity should be number." },
			min: { value: 0, msg: "Minimum  0." },
			max: { value: 99999999, msg: "Minimum  99999999." },
			required: { value: true, msg: "Quantity is required." }
		},
		errorMsg: "",
		valid: false,
		touched: false
	},
	// discountPercentage: {
	// 	elementType: "discountBox",
	// 	label: "Discount Percentage",

	// 	config: {
	// 		type: "discountBox",
	// 		value: {}
	// 	},
	// 	validation: {
	// 		required: { value: false, msg: "" }
	// 	},
	// 	errorMsg: "",
	// 	valid: true,
	// 	touched: false
	// },
	// discountAmount: {
	// 	elementType: "discountBox",
	// 	label: "Discount Amount",

	// 	config: {
	// 		type: "discountBox",
	// 		value: {}
	// 	},
	// 	validation: {
	// 		required: { value: false, msg: "" }
	// 	},
	// 	errorMsg: "",
	// 	valid: true,
	// 	touched: false
	// },
	// packagePricing: {
	// 	elementType: "packagePricingBox",
	// 	label: "Package price",

	// 	config: {
	// 		type: "packagePricingBox",
	// 		value: ""
	// 	},
	// 	validation: {
	// 		required: { value: false, msg: "" }
	// 	},
	// 	errorMsg: "",
	// 	valid: true,
	// 	touched: false
	// },
	images: {
		elementType: "file",
		label: "Image",
		config: {
			type: "file",
			value: []
		},
		validation: {
			type: {
				value: "file",
				valid: ["jpeg", "jpg", "png"],
				msg: "Max length 100"
			},
			required: { value: true, msg: "Images are mandatory." }
		},
		errorMsg: "",
		valid: false,
		touched: false
	}
};
