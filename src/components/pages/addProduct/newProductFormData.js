import styles from "./formStyles/formStyle.module.css";
import packagePricingStyle from "./formStyles/packagePricing.module.css";

export const productFormData = {
    separator1: {
        label: "PRIMARY INFO",
        elementType: "separator",

        config: {
            type: "title"
        },
        validation: {
            required: { value: false, msg: "" }
        },
        errorMsg: "",
        valid: "success",
        nonElement: true
    },
    name: {
        elementType: "text",
        label: "Product Name",
        config: {
            type: "text",
            placeholder: "Product Name",
            value: ""
        },
        validation: {
            type: { value: "string", msg: "Name should be in alphabet ." },
            maxLength: { value: 100, msg: "Maximum length 100." },
            minLength: { value: 3, msg: "Minimum length 3." },
            required: { value: true, msg: "Name is required." }
        },
        errorMsg: "Name is required. ",
        valid: "error",
        touched: false
    },
    brandName: {
        elementType: "text",
        label: "Brand",
        config: {
            type: "text",
            placeholder: "Brand",
            value: ""
        },
        validation: {
            type: { value: "string", msg: "Brand name should be in alphabet ." },
            maxLength: { value: 30, msg: "Maximum length 30." },
            minLength: { value: 1, msg: "Minimum length 1." },
            required: { value: true, msg: "Name is required." }
        },
        errorMsg: "Brand name is required. ",
        valid: "error",
        touched: false
    },
    model: {
        elementType: "text",
        label: "Model",
        config: {
            type: "text",
            placeholder: "Model Name",
            value: ""
        },
        validation: {
            type: { value: "string", msg: "Model should be in alphabet ." },
            maxLength: { value: 100, msg: "Maximum length 100." },
            minLength: { value: 1, msg: "Minimum length 1." },
            required: { value: true, msg: "Name is required." }
        },
        errorMsg: "Model is required. ",
        valid: "error",
        touched: false
    },
    shortDetails: {
        elementType: "textarea",
        label: "Short Details",

        config: {
            type: "text",
            placeholder: "Short Details",
            value: ""
        },
        validation: {
            type: { value: "string", msg: "Description  should be a text" },
            maxLength: { value: 100, msg: "Maximum 100 words." },
            minLength: { value: 4, msg: "Minimum  4." },
            required: { value: true, msg: "Short Details is required." }
        },
        errorMsg: "",
        valid: "error",
        touched: false
    },
    // thumbnailImage: {
    //     elementType: "image",
    //     label: "Thumbnail Image",
    //     config: {
    //         type: "image",
    //         length: 1,
    //         value: null
    //     },
    //     validation: {
    //         type: {
    //             value: "image",
    //             valid: ["jpeg", "jpg", "png"],
    //             msg: "Only JPEG JPG PNG allowed"
    //         },
    //         required: { value: true, msg: "Thumbnail image is mandatory." }
    //     },
    //     errorMsg: "Thumbnail image is mandatory.",
    //     valid: "error",
    //     touched: false
    // },
    thumbnailImages: {
        elementType: "image",
        label: "Thumbnail Image",
        config: {
          type: "file",
          value: []
        },
        validation: {
          type: {
            value: "file",
            valid: ["jpeg", "jpg", "png"],
            msg: "Max length 100"
          },
          required: { value: true, msg: "Thumbnail image is mandatory." }
        },
        errorMsg: "",
        valid: false,
        touched: false
      },
    separator2: {
        label: "DETAIL INFORMATIONS",
        elementType: "separator",

        config: {
            type: "title"
        },
        validation: {
            required: { value: false, msg: "" }
        },
        errorMsg: "",
        valid: "success",
        nonElement: true
    },
    description: {
        elementType: "textarea",
        label: "Description",

        config: {
            type: "text",
            placeholder: "Description",
            value: ""
        },
        validation: {
            type: { value: "string", msg: "Description  should be a text" },
            maxLength: { value: 100, msg: "Maximum 100 words." },
            minLength: { value: 4, msg: "Minimum  4." },
            required: { value: true, msg: "Short Details is required." }
        },
        errorMsg: "",
        valid: "error",
        touched: false
    },
    videoUrl: {
        elementType: "text",
        label: "Video Url",

        config: {
            type: "text",
            placeholder: "Video Url",
            value: ""
        },
        validation: {
            type: { value: "string", msg: "Url should be in alphabet." },
            maxLength: { value: 1000, msg: "Maximum length 1000." },
            minLength: { value: 3, msg: "Minimum length 3." },
            required: { value: false, msg: "" }
        },
        errorMsg: "",
        valid: "success",
        touched: false
    },
    tags: {
        elementType: "tags",
        label: "Tags",

        config: {
            type: "tags",
            submitMsg: "Add Tags",
            value: [],
            nested: {
                value: {
                    elementType: "text",
                    label: "Add Tag",

                    config: {
                        type: "text",
                        placeholder: "Tag",
                        value: ""
                    },
                    validation: {
                        type: { value: "string", msg: "Tag should be a text" },
                        maxLength: { value: 50, msg: "Maximum length 50." },
                        minLength: { value: 1, msg: "Minimum length 1." },
                        required: { value: true, msg: "" }
                    },
                    errorMsg: "",
                    valid: "error",
                    touched: false
                },
            },
        },
        validation: {
            type: { value: "array", msg: "" },
            maxLength: { value: 200, msg: "Maximum 200 tags allowed." },
            minLength: { value: 0, msg: "" },
            required: { value: false, msg: "" }
        },
        errorMsg: "",
        valid: "success",
        touched: false
    },

    shipping: {
        elementType: "nestedObject",
        label: "Shipping Information",

        config: {
            type: "nestedObject",
            value: {
                separator3: {
                    label: "SHIPPING INFORMATION",
                    elementType: "separator",
                    styles: styles,

                    config: {
                        type: "title"
                    },
                    validation: {
                        required: { value: false, msg: "" }
                    },
                    errorMsg: "",
                    valid: "success",
                    nonElement: true
                },
                height: {
                    elementType: "number",
                    label: "Package Height (Inch)",

                    config: {
                        type: "number",
                        placeholder: "Package Height (Inch)",
                        value: ""
                    },
                    validation: {
                        type: { value: "number", msg: "Height should be number." },
                        max: { value: 2000, msg: "Maximum 2000." },
                        min: { value: 0.001, msg: "Minimum  0.002." },
                        required: { value: false, msg: "" }
                    },
                    errorMsg: "",
                    valid: "success",
                    touched: false
                },
                width: {
                    elementType: "number",
                    label: "Package Width (Inch)",

                    config: {
                        type: "number",
                        placeholder: "Package Width (Inch)",
                        value: ""
                    },
                    validation: {
                        type: { value: "number", msg: "Width should be number." },
                        max: { value: 2000, msg: "Maximum 2000." },
                        min: { value: 0.001, msg: "Minimum  0.002." },
                        required: { value: false, msg: "" }
                    },
                    errorMsg: "",
                    valid: "success",
                    touched: false
                },
                length: {
                    elementType: "number",
                    label: "Package Length (Inch)",

                    config: {
                        type: "number",
                        placeholder: "Package Length (Inch)",
                        value: ""
                    },
                    validation: {
                        type: { value: "number", msg: "Length should be number." },
                        max: { value: 2000, msg: "Maximum 2000." },
                        min: { value: 0.001, msg: "Minimum  0.002." },
                        required: { value: false, msg: "" }
                    },
                    errorMsg: "",
                    valid: "success",
                    touched: false
                },
                weight: {
                    elementType: "number",
                    label: "Package Weight (kg)",

                    config: {
                        type: "number",
                        placeholder: "Package Weight (kg)",
                        value: ""
                    },
                    validation: {
                        type: { value: "number", msg: "Weight should be number." },
                        max: { value: 2000, msg: "Maximum 2000." },
                        min: { value: 0.001, msg: "Minimum  0.002." },
                        required: { value: true, msg: "Weight is required." }
                    },
                    errorMsg: "Weight is required.",
                    valid: "error",
                    touched: false
                },
                warehouseAddresses: {
                    elementType: "checkBox",
                    label: "Warehouse Addresses",

                    config: {
                        type: "select",
                        options: [
                            {
                                value: "khilkhet",
                                displayValue: "Khilkhet",
                                checked: false
                            },
                            {
                                value: "uttara",
                                displayValue: "Malibag",
                                checked: false
                            },
                            {
                                value: "mohammad pur",
                                displayValue: "Mohammad pur",
                                checked: false
                            }
                        ],
                        value: []
                    },
                    validation: {
                        type: { value: Array, msg: "" },
                        required: { value: false, msg: "" }
                    },
                    errorMsg: "",
                    valid: "success",
                    touched: false
                }

            }
        },
    }
}

export const variantFormData =  {
    separator4: {
        label: "VARIANTS INFORMATION",
        elementType: "separator",
        styles: styles,

        config: {
            type: "title"
        },
        validation: {
            required: { value: false, msg: "" }
        },
        errorMsg: "",
        valid: "success",
        nonElement: true
    },
    attrs: {
        elementType: "nestedObject",
        label: "ATTRIBUTES",

        config: {
            type: "nestedObject",
            value: {
                separator5: {
                    label: "ATTRIBUTES",
                    elementType: "separator",
                    styles: styles,

                    config: {
                        type: "title"
                    },
                    validation: {
                        required: { value: false, msg: "" }
                    },
                    errorMsg: "",
                    valid: "success",
                    nonElement: true
                },
                colorFamily: {
                    elementType: "select",
                    label: "Color Family",

                    config: {
                        type: "select",
                        options: [
                            { value: "green", displayValue: "Green" },
                            { value: "blue", displayValue: "Blue" },
                            { value: "black", displayValue: "Black" },
                            { value: "white", displayValue: "White" }
                        ],
                        value: "sdfg"
                    },
                    validation: {
                        type: { value: "string", msg: "Color family should be in alphabet." },
                        required: { value: true, msg: "Color family is required." }
                    },
                    errorMsg: "",
                    valid: "error",
                    touched: false
                },
                size: {
                    elementType: "select",
                    label: "Size",

                    config: {
                        type: "select",
                        options: [
                            { value: "xs", displayValue: "XS" },
                            { value: "sm", displayValue: "SM" },
                            { value: "md", displayValue: "MD" },
                            { value: "lg", displayValue: "LG" },
                            { value: "xl", displayValue: "XL" },
                            { value: "xxl", displayValue: "XXL" },
                            { value: "22", displayValue: "22" },
                            { value: "24", displayValue: "24" },
                            { value: "28", displayValue: "28" },
                            { value: "29", displayValue: "29" },
                            { value: "30", displayValue: "30" },
                            { value: "31", displayValue: "31" },
                            { value: "32", displayValue: "32" },
                            { value: "33", displayValue: "33" },
                            { value: "34", displayValue: "34" },
                            { value: "35", displayValue: "35" },
                            { value: "36", displayValue: "36" }
                        ],
                        value: ""
                    },
                    validation: {
                        type: { value: "string", msg: "Size should be in alphabet." },
                        required: { value: true, msg: "Size family is required." }
                    },
                    errorMsg: "",
                    valid: "error",
                    touched: false
                },
            }
        }
    },
    price: {
        elementType: "input",
        label: "Price",

        config: {
            type: "number",
            placeHolder: "Price",
            value: ""
        },
        validation: {
            type: { value: Number, msg: "Price should be number." },
            max: { value: 99999999, msg: "Maximum 99999999." },
            min: { value: 0, msg: "Minimum  0." },
            required: { value: true, msg: "Price is required." }
        },
        errorMsg: "",
        valid: "error",
        touched: false
    },
    quantity: {
        elementType: "input",
        label: "Quantity",

        config: {
            type: "number",
            placeHolder: "Quantity",
            value: ""
        },
        validation: {
            type: { value: Number, msg: "Quantity should be number." },
            min: { value: 0, msg: "Minimum  0." },
            max: { value: 99999999, msg: "Minimum  99999999." },
            required: { value: true, msg: "Quantity is required." }
        },
        errorMsg: "",
        valid: "error",
        touched: false
    },


    discountPercentage: {
        elementType: "nestedObject",
        label: "Discount in Percentage",

        config: {
            type: "number",
            placeHolder: "Discount Percentage",
            value: {
                separator3: {
                    label: "DISCOUNT PERCENTAGE",
                    elementType: "separator",
                    styles: styles,

                    config: {
                        type: "title"
                    },
                    validation: {
                        required: { value: false, msg: "" }
                    },
                    errorMsg: "",
                    valid: "success",
                    nonElement: true
                },
                amount: {
                    elementType: "number",
                    label: "Amount",

                    config: {
                        type: "number",
                        placeHolder: "Amount",
                        value: ""
                    },
                    validation: {
                        type: { value: "number", msg: "" },
                        max: { value: 99999999, msg: "Maximum 99999999." },
                        min: { value: 0, msg: "Minimum  0." },
                        required: { value: false, msg: "" }
                    },
                    errorMsg: "",
                    valid: "success",
                    touched: false
                },
                from: {
                    elementType: "date",
                    label: "Discount Percentage From",

                    config: {
                        type: "date",
                        placeHolder: "Discount Percentage From",
                        value: ""
                    },
                    validation: {
                        type: { value: "date", msg: "" },
                        // max: { value: 99999999, msg: "Maximum 99999999." },
                        // min: { value: 0, msg: "Minimum  0." },
                        required: { value: false, msg: "" }
                    },
                    errorMsg: "",
                    valid: "success",
                    touched: false
                },
                to: {
                    elementType: "date",
                    label: "Discount Percentage To",

                    config: {
                        type: "date",
                        placeHolder: "Discount Percentage To",
                        value: ""
                    },
                    validation: {
                        type: { value: "date", msg: "" },
                        // max: { value: 99999999, msg: "Maximum 99999999." },
                        // min: { value: 0, msg: "Minimum  0." },
                        required: { value: false, msg: "" }
                    },
                    errorMsg: "",
                    valid: "success",
                    touched: false
                }
            }
        },
        validation: {
            type: { value: "number", msg: "Discount percentage should be number." },
            max: { value: 99999999, msg: "Maximum 99999999." },
            min: { value: 0, msg: "Minimum  0." },
            required: { value: false, msg: "" }
        },
        errorMsg: "",
        valid: "success",
        touched: false
    },
    discountAmount: {
        elementType: "nestedObject",
        label: "Discount in Amount",

        config: {
            type: "number",
            placeHolder: "Discount Amount",
            value: {
                separator3: {
                    label: "DISCOUNT AMOUNT",
                    elementType: "separator",
                    styles: styles,

                    config: {
                        type: "title"
                    },
                    validation: {
                        required: { value: false, msg: "" }
                    },
                    errorMsg: "",
                    valid: "success",
                    nonElement: true
                },
                amount: {
                    elementType: "number",
                    label: "Amount",

                    config: {
                        type: "number",
                        placeHolder: "Amount",
                        value: ""
                    },
                    validation: {
                        type: { value: "number", msg: "" },
                        max: { value: 99999999, msg: "Maximum 99999999." },
                        min: { value: 0, msg: "Minimum  0." },
                        required: { value: false, msg: "" }
                    },
                    errorMsg: "",
                    valid: "success",
                    touched: false
                },
                from: {
                    elementType: "date",
                    label: "Discount Amount From",

                    config: {
                        type: "date",
                        placeHolder: "Discount Amount From",
                        value: ""
                    },
                    validation: {
                        type: { value: "date", msg: "" },
                        // max: { value: 99999999, msg: "Maximum 99999999." },
                        // min: { value: 0, msg: "Minimum  0." },
                        required: { value: false, msg: "" }
                    },
                    errorMsg: "",
                    valid: "success",
                    touched: false
                },
                to: {
                    elementType: "date",
                    label: "Discount Amount To",

                    config: {
                        type: "date",
                        placeHolder: "Discount Amount To",
                        value: ""
                    },
                    validation: {
                        type: { value: "date", msg: "" },
                        // max: { value: 99999999, msg: "Maximum 99999999." },
                        // min: { value: 0, msg: "Minimum  0." },
                        required: { value: false, msg: "" }
                    },
                    errorMsg: "",
                    valid: "success",
                    touched: false
                }
            }
        },
        validation: {
            type: { value: "number", msg: "Discount percentage should be number." },
            max: { value: 99999999, msg: "Maximum 99999999." },
            min: { value: 0, msg: "Minimum  0." },
            required: { value: false, msg: "" }
        },
        errorMsg: "",
        valid: "success",
        touched: false
    },
    packagePricing: {
        elementType: "nestedArray",
        label: "Package Pricing",
        styles: packagePricingStyle,

        config: {
            type: "nestedArray",
            submitMsg: "Add Package Pricing",
            value: [],
            nested: {
                separator5: {
                    label: "PACKAGE PRICING",
                    elementType: "separator",
                    styles: styles,

                    config: {
                        type: "title"
                    },
                    validation: {
                        required: { value: false, msg: "" }
                    },
                    errorMsg: "",
                    valid: "success",
                    nonElement: true
                },

                piece: {
                    elementType: "number",
                    label: "Piece",

                    config: {
                        type: "number",
                        placeholder: "Quantity",
                        value: 0
                    },
                    validation: {
                        type: { value: "number", msg: "Quantity should be number." },
                        min: { value: 2, msg: "Minimum  2." },
                        required: { value: true, msg: "" }
                    },
                    errorMsg: "",
                    valid: "error",
                    touched: false
                },

                price: {
                    elementType: "number",
                    label: "Price",

                    config: {
                        type: "number",
                        placeholder: "Quantity",
                        value: 0
                    },
                    validation: {
                        type: { value: "number", msg: "Quantity should be number." },
                        min: { value: 0, msg: "Minimum  0." },
                        required: { value: true, msg: "" }
                    },
                    errorMsg: "",
                    valid: "error",
                    touched: false
                }


            },
        },
        validation: {
            type: { value: "array", msg: "" },
            maxLength: { value: 100, msg: "Maximum 100 package pricing allowed." },
            minLength: { value: 0, msg: "" },
            required: { value: false, msg: "" }
        },
        errorMsg: "",
        valid: "success",
        touched: false
    },
    images: {
        elementType: "image",
        label: "Images",
        config: {
            type: "image",
            length: 5,
            value: null
        },
        validation: {
            type: {
                value: "image",
                valid: ["jpeg", "jpg", "png"],
                msg: "Only JPEG JPG PNG allowed"
            },
            required: { value: true, msg: "Images are mandatory." }
        },
        errorMsg: "Variant images are required.",
        valid: "error",
        touched: false
    }

}
