import React, { Fragment, useContext, useEffect } from "react";
import PageWrapper from '../../common/PageWrapper';
import LoadingSuspense from "../../common/LoadingSuspense";
import { PageHeader, Button, Input, Table, Icon } from 'antd';
import { ORDER_PATH, ORDER_INFO_PATH, EDIT_PATH } from "../../../routes/Slugs";
import { productFormData } from "../../pages/addProduct/newProductFormData";
import { Link, Redirect } from 'react-router-dom';
import { OrderContext } from "../../../contexts/OrderContextProvider";
import "./OrderInfo.scss"
const routes = [
    {
        path: `${ORDER_PATH}`,
        breadcrumbName: 'Order List',
    },
    {
        path: ORDER_INFO_PATH,
        breadcrumbName: 'Order Information',
    }
]

export default (props) => {

    const orderContext = useContext(OrderContext);
    // const orderInfo = orderContext.orderedProducts;
    const data = props.location.orderData;
    useEffect(() => {
        // data && orderContext.getAllOrderedProduct(data._id);
    }, []);

    const pageHeader = (data) && <PageHeader title={"Order Detail Information"} subTitle={`Id: ${data._id}`}
        breadcrumb={{ routes }}
    />
    console.log("datadatadatadatadata======", data)
    return (
        data ?
            <PageWrapper pageHeader={pageHeader}>



                {/* Render variants table */}
                <div className="internal_box">
                    <h4 className="internal_box_header">Ordered Products</h4>

                    <Table className="internal_table"
                        columns={orderedProductColoumn}
                        dataSource={data.orderedProducts.map((e, i) => ({ ...e, key: i }))}
                        pagination={false}
                    />

                </div>

                {/* Render shipping information */}


                {/* Render tags */}





            </PageWrapper > : <Redirect to={ORDER_PATH} />
    )
}




const orderedProductColoumn = [
    {
        title: "Order Product Id",
        dataIndex: "_id",
        key: "_id",
    },
    {
        title: "product Variant Id",
        dataIndex: "productVariantId",
        key: "productVariantId"
    },
    {
        title: "Quantity",
        dataIndex: "quantity",
        key: "quantity"
    },
    {
        title: 'Action',
        dataIndex: '',
        key: 'x',
        render: (e) => (
            <Fragment>
                <Link to={{ pathname: ORDER_INFO_PATH, data: e }}>
                    <Icon type="eye" />
                </Link>
                &nbsp;&nbsp;

            </Fragment>)
    },
]


