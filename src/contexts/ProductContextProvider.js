import React, { createContext, useState } from 'react';
import { GET_ALL_PRODUCT_API, POST_PRODUCT_API, GET_PRODUCT_ADDITIONAL_INFO, UPDATE_PRODUCT_SHIPPING, UPDATE_PRODUCT_VARIANT } from "../helpers/apis";
import axios from "axios";

export const ProductContext = createContext();

const ProductContextProvider = ({ children }) => {

    const [products, setProducts] = useState(null);
    const [productInfo, setProductInfo] = useState(null);


    const getAllProducts = async () => {
        try {
            const res = await axios.get(GET_ALL_PRODUCT_API);
            setProducts(res.data);
            return true;
        } catch (error) {
            console.log("products loading error.", error.response ? error.response.message : error.message)
            return false;
        }

    }


    const addProduct = async (data) => {

        try {
            await axios.post(POST_PRODUCT_API, data);
            setProducts([...products, data]);
            return true;
        } catch (error) {
            console.log("product post error.", error.response ? error.response.message : error.message);
            return false;
        }
    }

    const getProductAdditionalInfo = async (id) => {

        try {
            const res = await axios.get(GET_PRODUCT_ADDITIONAL_INFO + id);
            setProductInfo(res.data);
            return true;
        } catch (error) {
            console.log("product additional info getting error.", error)
            return false;
        }
    }


    return (
        <ProductContext.Provider
            value={{
                products,
                getAllProducts,
                getProductAdditionalInfo,
                productInfo,
                addProduct,
            }}
        >
            {children}
        </ProductContext.Provider>
    );
}

export default ProductContextProvider;