import React, { createContext, useState } from 'react';
import { GET_ALL_PRODUCTS, GET_ALL_ORDERED_PRODUCT, GET_DELIVERYINFO, GET_ORDER_STATUS } from "../helpers/apis";
import _ from "lodash";
import axios from "axios";

export const OrderContext = createContext();

const OrderContextProvider = ({ children }) => {

    const [orders, setOrders] = useState([]);
    const [orderedProducts, setOrderedProducts] = useState([]);
    const [deliveryInfo, setDeliveryInfo] = useState([]);
    const [orderStatus, setOrderStatus] = useState([]);
    const [errorMsg, setErrorMsg] = useState('');
    const [loading, setLoading] = useState(false);
    const [successMsg, setSuccessMsg] = useState("");

    const getAllOrders = () => {
        setLoading(true);
        axios.get(GET_ALL_PRODUCTS)
            .then(res => {
                console.log(res.data);
                setOrders(res.data);
                setLoading(false)
            })
            .catch(err => {
                setErrorMsg(err.response ? err.response.data : err.message)
                setLoading(false)
            });
    }

    const getAllOrderedProduct = id => {
        setLoading(true);
        axios.get(GET_ALL_ORDERED_PRODUCT + id)
            .then(res => {
                console.log(res.data);
                setOrderedProducts(res.data);
                setLoading(false)
            })
            .catch(err => {
                setErrorMsg(err.response ? err.response.data : err.message)
                setLoading(false)
            })
    }

    const getDeliveryInfo = id => {
        setLoading(true);
        axios.get(GET_DELIVERYINFO + id)
            .then(res => {
                console.log(res.data);
                setDeliveryInfo(res.data);
                setLoading(false)
            })
            .catch(err => {
                setErrorMsg(err.response ? err.response.data : err.message)
                setLoading(false)
            })
    }


    const getOrderStatusByCustomerId = id => {
        setLoading(true);
        axios.get(GET_ORDER_STATUS + id)
            .then(res => {
                console.log(_.pick(res.data, ["total", "completed", "canceled", "pending", "refund"]));
                setOrderStatus(res.data);
                setLoading(false)
            })
            .catch(err => {
                setErrorMsg(err.response ? err.response.data : err.message)
                setLoading(false)
            })
    }

    const resetStatus = () => {
        setErrorMsg('');
        setSuccessMsg('');
        setLoading(false);
    }

    return (
        <OrderContext.Provider
            value={{
                orders,
                orderedProducts,
                deliveryInfo,
                orderStatus,
                getAllOrders,
                getAllOrderedProduct,
                getDeliveryInfo,
                getOrderStatusByCustomerId,
                errorMsg,
                loading,
                successMsg,
                resetStatus
            }}
        >
            {children}
        </OrderContext.Provider>
    );
}

export default OrderContextProvider;